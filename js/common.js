new WOW().init({
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       100,          // default
    mobile:       true,       // default
    live:         true        // default
});

$(document).ready(function(){
    $('.feedback-slide').slick({
        nextArrow: '<img class="next-arrow" src="img/next-arrow.png" alt="">',
        prevArrow: '<img class="prev-arrow" src="img/prev-arrow.png" alt="">',
    });
});